import json
import csv
import os
import string
import re
import main

currentGame = main.game


def write_pokemon_json(poke_name):
    with open(currentGame + '/json/pokemon.json', 'r+', newline='\n') as pokemonJSON:
        if os.path.getsize(currentGame + '/json/pokemon.json') <= 0:
            json_obj = json.loads(json.dumps({"pokemon": []}))
        else:
            json_obj = json.loads(pokemonJSON.read())
        json_obj["pokemon"].append({'name': ''.join(filter(lambda x: x in printable, poke_name))})

    with open(currentGame + '/json/pokemon.json', 'w+', newline='\n') as pokemonJSON:
        pretty_print = json.dumps(json_obj, indent=4, sort_keys=True)
        pretty_print = re.sub('{\s+"n', '{ "n', pretty_print)
        pretty_print = re.sub('"\s+},', '" },', pretty_print)
        pretty_print = re.sub('"\s+}\s', '" }\n', pretty_print)
        pokemonJSON.write(pretty_print)


def write_pokemon_to_ability(poke, abil):
    with open(currentGame + '/json/pokemon_ability.json', 'r+', newline='\n') as abilityJSON:
        if os.path.getsize(currentGame + '/json/pokemon_ability.json') <= 0:
            json_obj = json.loads(json.dumps({abil: [{"pokemon": poke}]}))
        else:
            json_obj = json.loads(abilityJSON.read())
            if abil in json_obj:
                json_obj[abil].append({'pokemon': poke})
            else:
                json_obj[abil] = [{"pokemon": poke}]

    with open(currentGame + '/json/pokemon_ability.json', 'w+', newline='\n') as pokemonJSON:
        pretty_print = json.dumps(json_obj, indent=4, sort_keys=True)
        pretty_print = re.sub('{\s+"ability', '{ "ability', pretty_print)
        pretty_print = re.sub('{\s+"pokemon', '{ "pokemon', pretty_print)
        pretty_print = re.sub('"\s+},', '" },', pretty_print)
        pretty_print = re.sub('"\s+}\s', '" }\n', pretty_print)
        pokemonJSON.write(pretty_print)


def write_pokemon_to_move(poke, move, type):
    with open(currentGame + '/json/pokemon_move.json', 'r+', newline='\n') as abilityJSON:
        if os.path.getsize(currentGame + '/json/pokemon_move.json') <= 0:
            json_obj = json.loads(json.dumps({move: [{type: [{"pokemon": poke}]}]}))
        else:
            json_obj = json.loads(abilityJSON.read())
            if move in json_obj:
                for move_type in json_obj[move]:
                    if type in move_type:
                        found = False
                        for poke_name in move_type[type]:
                            if poke == poke_name['pokemon']:
                                found = True
                        if found:
                            pass
                        else:
                            move_type[type].append({'pokemon': poke})
                    else:
                        json_obj[move] = [{type: [{"pokemon": poke}]}]
            else:
                json_obj[move] = [{type: [{"pokemon": poke}]}]

    with open(currentGame + '/json/pokemon_move.json', 'w+', newline='\n') as pokemonJSON:
        pretty_print = json.dumps(json_obj, indent=4, sort_keys=True)
        pretty_print = re.sub('{\s+"move', '{ "move', pretty_print)
        pretty_print = re.sub('{\s+"level', '{ "level', pretty_print)
        pretty_print = re.sub('{\s+"pokemon', '{ "pokemon', pretty_print)
        pretty_print = re.sub('"\s+},', '" },', pretty_print)
        pretty_print = re.sub('"\s+}\s', '" }\n', pretty_print)
        pokemonJSON.write(pretty_print)


printable = set(string.printable)

exec(open('moves.py').read())
exec(open('abilities.py').read())

F = open(currentGame + '/csv/pokemon.csv', 'r+')

try:
    open(currentGame + '/json/pokemon.json', 'w+').close()
    open(currentGame + '/json/pokemon_ability.json', 'w+').close()
    open(currentGame + '/json/pokemon_move.json', 'w+').close()
    READER = csv.reader(F)
    for row in READER:
        index = 0
        pokemon = {}
        jsonTotal = {}
        fileName = row[0][3:] if not all(ord(char) < 128 for char in row[0]) else row[0]
        name = row[1]
        num = row[2]
        entry = row[3]
        types = []
        stats = []
        abilities = []
        moves = {}
        levelList = []
        tutorList = []
        tmList = []
        areas = {}
        walking = []
        surfing = []
        fishing = []
        event = []
        evolutions = []

        pokemon = {'name': name}
        write_pokemon_json(name)
        pokemon.update({'num': num})
        pokemon.update({'entry': entry})
        if entry == '':
            continue

        for i in range(0, 2):
            if row[4 + i] == '':
                break
            tmpType = {
                "type": row[4 + i]
            }
            index += 1
            types.append(tmpType)

        pokemon.update({'type_list': types})

        stats.append({'stat': 'Health', 'amount': row[6]})
        stats.append({'stat': 'Attack', 'amount': row[7]})
        stats.append({'stat': 'Defense', 'amount': row[8]})
        stats.append({'stat': 'Speed', 'amount': row[9]})
        stats.append({'stat': 'Special Attack', 'amount': row[10]})
        stats.append({'stat': 'Special Defense', 'amount': row[11]})

        pokemon.update({'stats': stats})

        with open(currentGame + '/json/abilities.json') as data_file:
            data = json.load(data_file)

        index = 12

        for i in range(0, 3):
            if row[index + i] == '':
                break
            for item in data['abilities']:
                if row[index + i] == item['ability']:
                    tmpAbility = {
                        'text': item['text'],
                        'ability': item['ability'],
                        'effect': item['effect']
                    }
                    abilities.append(tmpAbility)

        pokemon.update({'ability': abilities})
        for ability in abilities:
            write_pokemon_to_ability(name, ability['ability'])

        with open(currentGame + '/json/moves.json') as dataFile:
            data = json.load(dataFile)

        index = 15

        if row[index] == 'level':
            levelCount = int(row[index + 1])
            index = 17
            for i in range(1, levelCount * 2, 2):
                for item in data['moves']:
                    if row[index + i] == item['move_name']:
                        tmpMove = {
                            'move': item['move_name'],
                            'lvl': row[index + i - 1],
                            'type': item['type'],
                            'catagory': item['catagory'],
                            'pp': item['pp'],
                            'power': item['power'],
                            'accuracy': item['accuracy']
                        }
                        levelList.append(tmpMove)
                        write_pokemon_to_move(name, item['move_name'], 'level')
            moves.update({'level': levelList})
            index = 18 + levelCount * 2
        else:
            moves.update({'level': []})

        with open(currentGame + '/json/tmMoves.json') as dataFile:
            tmData = json.load(dataFile)

        if row[index - 1] == 'TM':
            tmCount = int(row[index]) + 1
            for i in range(1, tmCount):
                for item in tmData['moves']:
                    if row[index + i] == item['move_name']:
                        for moveItem in data['moves']:
                            if item['move_name'] == moveItem['move_name']:
                                tmpTM = {
                                    'tm': item['tm'],
                                    'move': item['move_name'],
                                    'type': moveItem['type'],
                                    'catagory': moveItem['catagory'],
                                    'pp': moveItem['pp'],
                                    'power': moveItem['power'],
                                    'accuracy': moveItem['accuracy']
                                }
                                tmList.append(tmpTM)
            moves.update({'tm': tmList})
            index += tmCount
        else:
            moves.update({'tm', []})

        if row[index] == 'Tutor':
            tutorCount = int(row[index + 1])
            for i in range(0, tutorCount):
                tmpTutor = {
                    'move': row[index + i + 2]
                }
                tutorList.append(tmpTutor)
            moves.update({'tutor': tutorList})
            index += tutorCount + 2
        else:
            moves.update({'tutor': []})

        pokemon.update({'moves': moves})

        if row[index] == 'Walking':
            walkCount = int(row[index + 1])
            for i in range(2, walkCount + 2, 2):
                tmpWalk = {
                    'area': row[index + i],
                    'percent': row[index + i + 1]
                }
                walking.append(tmpWalk)
            areas.update({'walking': walking})
            index += walkCount * 2 + 2
        else:
            areas.update({'walking': []})

        if row[index] == 'Surfing':
            surfCount = int(row[index + 1])
            for i in range(2, surfCount + 2, 2):
                tmpSurf = {
                    'area': row[index + i],
                    'percent': row[index + i + 1]
                }
                surfing.append(tmpSurf)
            areas.update({'surfing': surfing})
            index += surfCount * 2 + 2
        else:
            areas.update({'surfing': []})

        if row[index] == 'Fishing':
            fishCount = int(row[index + 1])
            for i in range(2, fishCount + 2, 2):
                tmpFish = {
                    'area': row[index + i],
                    'percent': row[index + i + 1]
                }
                fishing.append(tmpFish)
            areas.update({'fishing': fishing})
            index += fishCount * 2 + 2
        else:
            areas.update({'fishing': []})

        if row[index] == 'Event':
            eventCount = int(row[index + 1])
            for i in range(2, eventCount + 2, 2):
                tmpEvent = {
                    'area': row[index + i],
                    'percent': row[index + i + 1]
                }
                event.append(tmpEvent)
            areas.update({'event': event})
            index += eventCount * 2 + 2
        else:
            areas.update({'event': []})

        pokemon.update({'areas': areas})

        if row[index] == 'Evolutions':
            evoCount = int(row[index + 1])
            index += 2
            for i in range(0, evoCount * 3, 3):
                tmpEvo = {
                    'num': row[index + i],
                    'name': row[index + i + 1],
                    'level': row[index + 2 + i]
                }
                evolutions.append(tmpEvo)
            pokemon.update({'evolutions': evolutions})
        else:
            pokemon.update({'evolutions': []})

        jsonTotal = {'pokemon': pokemon}

        jsonStr = json.dumps(jsonTotal, indent=2)

        textFile = open(currentGame + '/json/pokemon/' + fileName, 'w+')
        textFile.write(jsonStr)
        textFile.close()
finally:
    F.close()
