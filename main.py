import os

games = ['vega']

for game in games:
    if not os.path.exists(game):
        os.makedirs(game)
    if not os.path.exists(game + '/csv'):
        os.makedirs(game + '/csv')
    if not os.path.exists(game + '/json'):
        os.makedirs(game + '/json')
    if not os.path.exists(game + '/json/pokemon'):
        os.makedirs(game + '/json/pokemon')
    exec(open('create.py').read())
