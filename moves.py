import csv
import json
import main

f = open(main.game + '/csv/moves.csv', 'r')

dataList = []

try:
    reader = csv.reader(f)
    for row in reader:
        data = {
            'move_name': row[0],
            'type': row[1],
            'catagory': row[2],
            'power': row[3],
            'accuracy': row[4],
            'pp': row[5]
        }
        dataList.append(data)
finally:
    f.close()

dataList.pop(0)

moveList = {'moves': dataList}

json_str = json.dumps(moveList, indent=2)

textFile = open(main.game + '/json/moves.json', 'w+')
textFile.write(json_str)
textFile.close()

exec(open('tmMoves.py').read())
