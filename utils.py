import csv
import re
import types

with open('csv/pokemon.csv', 'r+') as file:
    dexSplit = []
    with open("Vega Dex.txt", "r+") as dex:
        for line in dex:
            tmp_split = line.split("\t")
            tmp_split[:] = ((list(val.strip() for val in value.split("/") if val) if re.search("/", value) else value)
                            for value in tmp_split if not value == '')
            if '\n' not in tmp_split:
                dexSplit.append(tmp_split)
                print(tmp_split)

    newFile = []
    reader = csv.reader(file)
    index = 0
    for row in reader:
        newFileRow = row
        dexEntry = dexSplit[index]

        if isinstance(dexEntry[2], list):
            for type in dexEntry[2]:
                newFileRow.append(type)
        else:
            newFileRow.append(dexEntry[2])
            newFileRow.append('')

        for stat in dexEntry[4]:
            newFileRow.append(stat)

        if isinstance(dexEntry[3], list):
            for ability in dexEntry[3]:
                newFileRow.append(ability)
            if len(dexEntry[3]) == 2:
                newFileRow.append('')
        else:
            newFileRow.append(ability)
            newFileRow.append('')
            newFileRow.append('')

        newFile.append(newFileRow)
        index += 1

    writer = csv.writer(open('csv/pokemon.csv', 'w+', newline='\n'))
    for item in newFile:
        writer.writerow(item)
