import csv
import json
import main

F = open(main.game + '/csv/tmMoves.csv', 'r')

tmList = []
moveList = {}

try:
    READER = csv.reader(F)

    for row in READER:
        tmpData = {
            'tm': row[0],
            'move_name': row[1]
        }
        tmList.append(tmpData)

    tmList.pop(0)
    moveList = {'moves': tmList}
finally:
    F.close()

json_str = json.dumps(moveList, indent=2)

textFile = open(main.game + '/json/tmMoves.json', 'w+')
textFile.write(json_str)
textFile.close()
