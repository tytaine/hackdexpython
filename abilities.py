import json
import csv
import main

F = open(main.game + '/csv/abilities.csv', 'r')

dataList = []

try:
    READER = csv.reader(F)
    for row in READER:
        data = {
            'ability': row[0],
            'text': row[1],
            'effect': row[2]
        }
        dataList.append(data)
finally:
    F.close()

dataList.pop(0)

abilityList = {'abilities': dataList}

jsonStr = json.dumps(abilityList, indent=4)

textFile = open(main.game + '/json/abilities.json', 'w+')
textFile.write(jsonStr)
textFile.close()